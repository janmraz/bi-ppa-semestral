bigger(rex, akos).
bigger(andak, meda).
bigger(akos, meda).
bigger(meda, cernoch).
bigger(cernoch, mys).

test(ahoj, ahoj).
test(ahoj, hle).
testD(X, Y) :- test(X, Y).

isBigger(X, X) :- fail.
isBigger(X, Y) :- bigger(X, Y).
isBigger(X, Y) :- bigger(X, Z), isBigger(Z, Y).

sameSize(X, Y) :- \+isBigger(X, Y), \+isBigger(Y, X).

% ======= Databaze faktu ========
man(bart).
man(homer).
man(abraham).
man(clancy).
woman(mona).
woman(marge).
woman(liza).
woman(meggie).
woman(selma).
woman(patty).
woman(jacqueline).
parent(homer,bart).
parent(homer,liza).
parent(homer,meggie).
parent(abraham,homer).
parent(marge,bart).
parent(marge,liza).
parent(marge,meggie).
parent(mona,homer).
parent(jacqueline,marge).
parent(jacqueline,patty).
parent(jacqueline,selma).
married(homer,marge).
married(abraham,mona).
married(clancy,jacqueline).


father(X, Y) :- man(X), parent(X, Y).
mother(X, Y) :- woman(X), parent(X, Y).
son(S, X) :- man(S), parent(X, S).
daughter(S, X) :- woman(S), parent(X, S).

sibling(S, S) :- fail.
sibling(X, Y) :- parent(Z, X), parent(Z, Y).

grandparent(G, X) :- parent(Z, X), parent(G, Z).

aunt(A, X) :- woman(A), parent(Z, X), sibling(A, Z), A\=Z.

father_in_law(F, X) :- married(F, M), mother(M, X), \+ father(F, X).


% bod(X, Y).
% usecka(Zacatek, Konec)
% usecka(bod(X, Y), bod(U, V)).

svisla(usecka(bod(X, _), bod(X, _))).
vodorovna(usecka(bod(_, Y), bod(_, Y))).

% reverse
rever(L1, L2) :- rever(L1, [], L2).
rever([], A, A).
rever([X|T], A, L2) :- rever(T, [X|A], L2). 

% factorial
% recursively call factorial until the end condition
fact(0, [1]) :- !.
fact(N, [Res, Res2|T]) :- N_1 is N - 1, fact(N_1, [Res2| T]), Res is Res2*N.

fact_r(N, Res) :- fact(N, Res1), rever(Res1, Res).


%min
%min([X], X).
%min([X|T], Res) :- min(T, Res1), Res > Res1, Res is Res1.
min([X|T], Res) :- min(T, X, Res).
min([], A, A).
min([X|T], A, Res) :- X < A, min(T, X, Res), !.
min([X|T], A, Res) :- X >= A, min(T, A, Res).


% contains
contains([X], X).
contains([H|_], H).
contains([_|T], Res) :- contains(T, Res).

% nth
nth([H|_], 0, H).
nth([_|T], N, Res) :- N1 is N - 1, nth(T, N1, Res), !.

% length
mylength([], 0).
mylength([_|T], Res) :- mylength(T, Res1), Res is Res1 + 1.

myLengthRec([], 0).
myLengthRec([H|T], Res) :-   atomic(H),!,                       myLengthRec(T, Res1), Res is Res1 + 1.
myLengthRec([H|T], Res) :- \+atomic(H),!, myLengthRec(H, Res2), myLengthRec(T, Res1), Res is Res1 + Res2.


mappend([], A, [A]).
mappend([H|T], A, [H|Res]) :- mappend(T,A,Res).

prepend(L, I, [I|L]).

replace([], _, _, []).
replace([X], F, _, [X]) :- X\=F, !.
replace([X], X, S, [S]) :- !.
replace([H|T], H, S, [S|Res]) :- replace(T, H, S, Res), !.
replace([H|T], I, S, [H|Res]) :- replace(T, I, S, Res), !.

deleteFirst([H|T], H, T) :-  !.
deleteFirst([H|T], I, [H|Res]) :- deleteFirst(T, I, Res), !.

deleteLast(L, I, Res) :- rever(L, LR), deleteFirst(LR, I, Res2), rever(Res2, Res).

deleteAll([], _, []).
deleteAll([X], F, [X]) :- X\=F, !.
deleteAll([X], X, []) :- !.
deleteAll([H|T], H, Res) :- deleteAll(T, H, Res), !.
deleteAll([H|T], I, [H|Res]) :- deleteAll(T, I, Res), !.


last([X], X) :- !.
last([_|T], Res) :- last(T, Res), !. 

butlast([_], []) :- !.
butlast([X|T], [X|Res]) :- butlast(T, Res), !. 

count([], _, 0) :- !.
count([X], X, 1) :- !.
count([D], X, 0) :- X \= D, !. 
count([X|T], X, Res) :- count(T, X, Res1), Res is Res1 + 1, !.
count([X|T], F, Res) :- X \= F, count(T, F, Res1), Res is Res1, !.


merge(A, [] ,A) :- !.
merge(L1, [X|T], Res) :- mappend(L1, X, R), merge(R, T, Res), !.

flatten([],[]) :- !.
flatten([X|T], [X|R]) :- atomic(X), flatten(T, R), !.
flatten([X|T], R) :- \+atomic(X), flatten(X, Res), flatten(T, Re), merge(Res, Re, R), !.

sorted([]) :- !.
sorted([X]) :- !.
sorted([X, Y| T]) :- X < Y, sorted([Y|T]) ,!.


secondMin([], ) :- !.
secondMin([X|T], Res) :- secondMin(T, Res, X, false).

secondMin([], SM, M, SM) :- SM = false, fail.
secondMin([], SM, M, SM) :- SM \= false, !. 

secondMin([X|T], Res, M, SM) :- X < M,  secondMin(T, Res, X, M), !.
secondMin([X|T], Res, M, SM) :- X > M, X < SM, secondMin(T, Res, M, X), !.
secondMin([X|T], Res, M, SM) :- X > M, X >= SM, secondMin(T, Res, M, SM), !.
secondMin([X|T], Res, M, SM) :- X = M, secondMin(T, Res, M, SM), !.

fib(0, [1]) :- !.
fib(1, [1,1]) :- !.
fib(N, [C, X|Res]) :- N1 is N-1, N2 is N-2, fib(N1, [X |Res]), fib(N2, [Y| Res2]), C is X + Y, !.


zero([]).
succesor(X, Res) :- mappend(X, x, Res). 
add(L1, L2, Res) :- merge(L1, L2, Res).
mult(L1, L2, Res) :- mylength(L2, LM), mult(L1, L2, Res, LM), !.

mult(L1, L2, L2, 0) :- !.
mult(L1, L2, Res, A) :- add(L1, L2, Res), A1 is A-1, mult(L1, L2, Res, A1), !.

%is_in(I, []) :- fail.
is_in([[X,_] | _], X) :- !.
is_in([[X,_] | T], I) :- X \= I, is_in(T, I), !.

add_in(L, H, [[H, 1]|L]).

inc_in([[X,Y] | T], X, [[X,Y1] | T]) :- Y1 is Y + 1, !.
inc_in([[X,Y] | T], I, [[X, Y]|Res]) :- inc_in(T, I, Res), !.

hist(L, Res) :- hist(L, [], Res).
hist([], A, A) :- !.
hist([H|T], A, R) :- is_in(A, H)  , inc_in(A, H, R1), hist(T, R1, R).
hist([H|T], A, R) :- \+is_in(A, H), add_in(A, H, R1), hist(T, R1, R).

%removeMin([H|T],M, Res) :- removeMin(T, M, [], Res).

removeMin([H|T], H, T) :- !.
removeMin([H|T], I, [H|Res]) :- H \= I, removeMin(T, I, Res). 

selectSort(Lst, Lst) :- sorted(Lst), !.
selectSort(Lst, [Min| Rest]) :- min(Lst, Min), removeMin(Lst, Min, Res), selectSort(Res, Rest), !.
 
bvs(5, bvs(3, nil, nil), bvs(8, bvs(7, nil, nil), bvs(9, nil, nil))).

bvsSearch(bvs(C, L, R), C) :- !.
bvsSearch(bvs(C, L, R), D) :- C \= D, C > D, bvsSearch(R, D), !.
bvsSearch(bvs(C, L, R), D) :- C \= D, C < D, bvsSearch(L, D), !.

bvsInsert(bvs(C, nil, R), E, bvs(C, bvs(E, nil, nil), R)) :- E < C, !.
bvsInsert(bvs(C, nil, nil), E, bvs(C, bvs(E, nil, nil), nil)) :- E < C, !.
bvsInsert(bvs(C, nil, nil), E, bvs(C, nil, bvs(E, nil, nil))) :- E > C, !.
bvsInsert(bvs(C, L, nil), E, bvs(C, L, bvs(E, nil, nil))) :- E > C,  !.
bvsInsert(bvs(C, L, R), E, bvs(C, L, RM)) :- E > C, bvsInsert(R, E, RM), !.
bvsInsert(bvs(C, L, R), E, bvs(C, LM, R)) :- E < C, bvsInsert(L, E, LM), !.

bvsInOrder(bvs(C,nil, nil), [C]) :- !.
bvsInOrder(bvs(C, L, nil), Result) :- bvsInOrder(L, Res), merge(Res, [C], Result), !.
bvsInOrder(bvs(C,nil, R), Result) :- bvsInOrder(R, Res), merge([C], Res, Result), !.
bvsInOrder(bvs(C,L,R), Result) :- bvsInOrder(L, ResL),bvsInOrder(R, ResR), merge(ResL, [C], RLC), merge(RLC, ResR, Result), !.

%MatDim(+Mat, -Rows, -Cols)
%[[1,2,3,4], [1,2,3,4], [1,2,3,4]]

matDim([R | Rest], Rows, Cols) :- length([R | Rest], Rows), length(R, Cols), !.

matNthRow([H | T], 0, H) :- !.
matNthRow([H | T], N, Res) :- N \= 0, N1 is N-1, matNthRow(T, N1, Res), !. 




